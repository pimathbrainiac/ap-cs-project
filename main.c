#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "main.h"

//Screen dimension constants
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;
const int TILE_WIDTH = 32;
const int TILE_HEIGHT = 32;
//number of sprites
const int NUM_SPRITES = 16;
//number of minotaur frames
const int NUM_FRAMES_MINOTAUR = 4;
//number of torches
const int NUM_TORCHES = 6;

//for timings in the animations and key interrupts
int numFrames = 0;

//to check if the player can move in different directions
int canMoveRight = 0;
int canMoveLeft = 0;
int canMoveUp = 0;
int canMoveDown = 0;

//to check if the minotaur can move in different directions
int minotaurCanMoveRight = 0;
int minotaurCanMoveLeft = 0;
int minotaurCanMoveUp = 0;
int minotaurCanMoveDown = 0;
int lastMinotaurMovement = MINOTAUR_DIRECTION_UP;

//to check which directions the player or minotaur can move on a given tile type
tileDirections tiles[16];

int playerSpotted = 0;

//player rect
SDL_Rect player;

//minotaur rect
SDL_Rect minotaur;

SDL_Rect plunger;

//player frame rect
SDL_Rect playerFrame;

//minotaur frame rect
SDL_Rect minotaurFrame;

//torch frame rect
SDL_Rect torchFrame;

//map data
int map[24][32] = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

int torchMap[24][32] = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

//Loads map
int load_map(char*);

//Reads the map data and stores in map
void read_map();

//Loads media
int load_media();

//Renders the screen
void render();

//Starts up SDL and creates window
int init();

//Frees media and shuts down SDL
void close();

//places torches
void place_torches(int);

//move the minotaur
void move_minotaur(int);

Uint32 animate(Uint32, void*);

//CSV file for the map
FILE *mapCSV;

//The window we'll be rendering to
SDL_Window *gWindow = NULL;

//The surface contained by the window
SDL_Surface *gScreenSurface = NULL;

//The spritesheet
SDL_Surface *gSprites = NULL;

//The player sprite frames
SDL_Surface *gPlayer = NULL;

//The minotaur sprite frames
SDL_Surface *gMinotaur = NULL;

//The torch sprite frames
SDL_Surface *gTorch = NULL;

//The plunger sprite
SDL_Surface *gPlunger = NULL;

int main(int argc, char** argv)
{
    srand(time(NULL));
    if (!init())
    {
        exit(EXIT_FAILURE);
    }
    if (!load_media())
    {
        exit(EXIT_FAILURE);
    }
    SDL_UpdateWindowSurface(gWindow);
    int quit = 0;
    SDL_Event e;
    render();
    Uint32 delay = 67;
    SDL_TimerID animTimer = SDL_AddTimer(delay, animate, "useless param");
    while (!quit)
    {
        while (SDL_PollEvent(&e) != 0)
        {
            if (e.type == SDL_QUIT)
            {
                quit = 1;
            }
        }
        const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
        if (currentKeyStates[SDL_SCANCODE_ESCAPE])
        {
            quit = 1;
        }
        if (minotaur.x == player.x && minotaur.y == player.y)
        {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "You Lost! :(", "You lost. Try Again!", NULL);
            quit = 1;
        }
        else if (player.x == plunger.x && player.y == plunger.y)
        {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "WINNER!!!", "You win. Have a cookie!", NULL);
            quit = 1;
        }
        render();
    }
    SDL_RemoveTimer(animTimer);
    close();
    return EXIT_SUCCESS;
}

int load_map(char *type)
{
    mapCSV = fopen("map.csv", type);
    return mapCSV;
}

void read_map()
{
    int row = 0;
    int col = 0;
    for (row = 0; row < 24; row++)
    {
        for (col = 0; col < 32; col++)
        {
            fscanf(mapCSV, "%d, ", &map[row][col]);
        }
    }
}

void render()
{
    SDL_FillRect(gScreenSurface, NULL, 0x000000);
    int row = 0;
    int col = 0;
    SDL_Rect sprite;
    sprite.x = 0;
    sprite.y = 0;
    sprite.w = TILE_WIDTH;
    sprite.h = TILE_HEIGHT;
    SDL_Rect tile;
    tile.x = 0;
    tile.y = 0;
    tile.w = TILE_WIDTH;
    tile.h = TILE_HEIGHT;
    if (torchMap[player.y / 32][player.x / 32] > 0)
    {
        playerSpotted = 1;
    }
    if (playerSpotted)
    {
        SDL_BlitSurface(gMinotaur, &minotaurFrame, gScreenSurface, &minotaur);
    }
    for (row = 0; row < 24; row++)
    {
        for (col = 0; col < 32; col++)
        {
            if (torchMap[row][col] > 0)
            {
                sprite.x = map[row][col] * 32;
                tile.x = col * 32;
                tile.y = row * 32;
                SDL_BlitSurface(gSprites, &sprite, gScreenSurface, &tile);
                if (torchMap[row][col] == 1)
                {
                    SDL_BlitSurface(gTorch, &torchFrame, gScreenSurface, &tile);
                }
                if((col == (minotaur.x / 32) && row == (minotaur.y / 32)) && !playerSpotted)
                {
                    SDL_BlitSurface(gMinotaur, &minotaurFrame, gScreenSurface, &minotaur);
                }
                if (col == (plunger.x / 32) && row == (plunger.y / 32))
                {
                    SDL_BlitSurface(gPlunger, NULL, gScreenSurface, &plunger);
                }
            }
            else if (col >= (player.x - 32) / 32 && col <= (player.x + 32) / 32 && row >= (player.y - 32) / 32 && row <= (player.y + 32) / 32)
            {
                sprite.x = map[row][col] * 32;
                tile.x = col * 32;
                tile.y = row * 32;
                SDL_BlitSurface(gSprites, &sprite, gScreenSurface, &tile);
                if (col == (minotaur.x / 32) && row == (minotaur.y / 32))
                {
                    SDL_BlitSurface(gMinotaur, &minotaurFrame, gScreenSurface, &minotaur);
                }
                if (col == (plunger.x / 32) && row == (plunger.y / 32))
                {
                    SDL_BlitSurface(gPlunger, NULL, gScreenSurface, &plunger);
                }
            }
        }
    }
    
    //In which Directions can the player move, given its current tile?
    canMoveRight    =   tiles[map[player.y / 32][player.x / 32]].moveRight;
    canMoveLeft     =   tiles[map[player.y / 32][player.x / 32]].moveLeft;
    canMoveUp       =   tiles[map[player.y / 32][player.x / 32]].moveUp;
    canMoveDown     =   tiles[map[player.y / 32][player.x / 32]].moveDown;
    
    //In which Directions can the player move, given its current tile?
    minotaurCanMoveRight    =   tiles[map[minotaur.y / 32][minotaur.x / 32]].moveRight;
    minotaurCanMoveLeft     =   tiles[map[minotaur.y / 32][minotaur.x / 32]].moveLeft;
    minotaurCanMoveUp       =   tiles[map[minotaur.y / 32][minotaur.x / 32]].moveUp;
    minotaurCanMoveDown     =   tiles[map[minotaur.y / 32][minotaur.x / 32]].moveDown;
    
    SDL_BlitSurface(gPlayer, &playerFrame, gScreenSurface, &player);
    if (playerSpotted)
    {
        SDL_BlitSurface(gPlunger, NULL, gScreenSurface, &plunger);
    }
    SDL_UpdateWindowSurface(gWindow);
}

int init()
{
    int success = 1;
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL failed to init video!");
        success = 0;
    }
    else
    {
        gWindow = SDL_CreateWindow("Quest for the Golden Plunger", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (gWindow == NULL)
        {
            printf("SDL failed to create Window!");
            success = 0;
        }
        else
        {
            gScreenSurface = SDL_GetWindowSurface(gWindow);
            if (!load_map("r"))
            {
                printf("Failed to load/create the map CSV file");
                fclose(mapCSV);
                success = 0;
            }
            else
            {
                read_map();
                fclose(mapCSV);
                place_torches(NUM_TORCHES);
            }
        }
    }
    
    //to check which directions the player or minotaur can move on a given tile type
    tiles[0].moveRight  =   1;
    tiles[0].moveLeft   =   1;
    tiles[0].moveUp     =   1;
    tiles[0].moveDown   =   1;
    tiles[1].moveRight  =   1;
    tiles[1].moveLeft   =   1;
    tiles[1].moveUp     =   1;
    tiles[1].moveDown   =   0;
    tiles[2].moveRight  =   1;
    tiles[2].moveLeft   =   1;
    tiles[2].moveUp     =   0;
    tiles[2].moveDown   =   0;
    tiles[3].moveRight  =   0;
    tiles[3].moveLeft   =   0;
    tiles[3].moveUp     =   1;
    tiles[3].moveDown   =   1;
    tiles[4].moveRight  =   1;
    tiles[4].moveLeft   =   0;
    tiles[4].moveUp     =   1;
    tiles[4].moveDown   =   1;
    tiles[5].moveRight  =   0;
    tiles[5].moveLeft   =   1;
    tiles[5].moveUp     =   0;
    tiles[5].moveDown   =   1;
    tiles[6].moveRight  =   1;
    tiles[6].moveLeft   =   0;
    tiles[6].moveUp     =   0;
    tiles[6].moveDown   =   1;
    tiles[7].moveRight  =   0;
    tiles[7].moveLeft   =   1;
    tiles[7].moveUp     =   1;
    tiles[7].moveDown   =   1;
    tiles[8].moveRight  =   0;
    tiles[8].moveLeft   =   1;
    tiles[8].moveUp     =   1;
    tiles[8].moveDown   =   0;
    tiles[9].moveRight  =   1;
    tiles[9].moveLeft   =   1;
    tiles[9].moveUp     =   0;
    tiles[9].moveDown   =   1;
    tiles[10].moveRight =   1;
    tiles[10].moveLeft  =   0;
    tiles[10].moveUp    =   1;
    tiles[10].moveDown  =   0;
    tiles[11].moveRight =   1;
    tiles[11].moveLeft  =   1;
    tiles[11].moveUp    =   1;
    tiles[11].moveDown  =   1;
    tiles[12].moveRight =   0;
    tiles[12].moveLeft  =   1;
    tiles[12].moveUp    =   0;
    tiles[12].moveDown  =   0;
    tiles[13].moveRight =   1;
    tiles[13].moveLeft  =   0;
    tiles[13].moveUp    =   0;
    tiles[13].moveDown  =   0;
    tiles[14].moveRight =   0;
    tiles[14].moveLeft  =   0;
    tiles[14].moveUp    =   1;
    tiles[14].moveDown  =   0;
    tiles[15].moveRight =   0;
    tiles[15].moveLeft  =   0;
    tiles[15].moveUp    =   0;
    tiles[15].moveDown  =   1;
    
    return success;
}

int load_media()
{
    int success = 1;
    gMinotaur = SDL_LoadBMP("minotaur.bmp");
    if (gMinotaur == NULL)
    {
        success = 0;
    }
    gSprites = SDL_LoadBMP("sprites.bmp");
    if (gSprites == NULL)
    {
        success = 0;
    }
    gTorch = SDL_LoadBMP("torch.bmp");
    if (gTorch == NULL)
    {
        success = 0;
    }
    gPlayer = SDL_LoadBMP("player.bmp");
    if (gPlayer == NULL)
    {
        success = 0;
    }
    gPlunger = SDL_LoadBMP("plunger.bmp");
    if (gPlunger == NULL)
    {
        success = 0;
    }
    plunger.x = (rand() % 32) * 32;
    plunger.y = (rand() % 24) * 32;
    player.w = 32;
    player.h = 32;
    player.x = 0;
    player.y = 0;
    player.w = TILE_WIDTH;
    player.h = TILE_HEIGHT;
    minotaur.x = 992;
    minotaur.y = 736;
    minotaur.w = TILE_WIDTH;
    minotaur.h = TILE_HEIGHT;
    playerFrame.x = 0;
    playerFrame.y = 0;
    playerFrame.w = TILE_WIDTH;
    playerFrame.h = TILE_HEIGHT;
    minotaurFrame.x = 0;
    minotaurFrame.y = 0;
    minotaurFrame.w = TILE_WIDTH;
    minotaurFrame.h = TILE_HEIGHT;
    torchFrame.x = 0;
    torchFrame.y = 0;
    torchFrame.w = TILE_WIDTH;
    torchFrame.h = TILE_HEIGHT;
    
    //Sets the color that when rendered, will be transparent
    SDL_SetColorKey(gSprites, SDL_TRUE, 0x00FFFF);
    SDL_SetColorKey(gPlayer, SDL_TRUE, 0x00FFFF);
    SDL_SetColorKey(gMinotaur, SDL_TRUE, 0x00FFFF);
    SDL_SetColorKey(gTorch, SDL_TRUE, 0x00FFFF);
    SDL_SetColorKey(gPlunger, SDL_TRUE, 0x00FFFF);
    return success;
}

void close()
{
    //Free all the surfaces
    SDL_FreeSurface(gSprites);
    gSprites = NULL;
    SDL_FreeSurface(gPlayer);
    gPlayer = NULL;
    SDL_FreeSurface(gMinotaur);
    gMinotaur = NULL;
    SDL_FreeSurface(gTorch);
    gTorch = NULL;
    SDL_FreeSurface(gPlunger);
    gPlunger = NULL;

    //Destroy window
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;
    //Quit SDL subsystems
    SDL_Quit();
}

Uint32 animate(Uint32 interval, void *param)
{
    const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
    if (!(numFrames % 7))
    {
        if (torchFrame.x == 0)
        {
            torchFrame.x = 32;
        }
        else
        {
            torchFrame.x = 0;
        }
        if (playerFrame.x == 0)
        {
            playerFrame.x = 32;
        }
        else
        {
            playerFrame.x = 0;
        }
    }
    if (!(numFrames % 3))
    {
        if (currentKeyStates[SDL_SCANCODE_UP] && player.y > 0 && canMoveUp && !currentKeyStates[SDL_SCANCODE_DOWN] && !currentKeyStates[SDL_SCANCODE_LEFT] && !currentKeyStates[SDL_SCANCODE_RIGHT])
        {
            player.y -= 32;
        }
        else if (currentKeyStates[SDL_SCANCODE_DOWN] && player.y < SCREEN_HEIGHT - 32 && canMoveDown && !currentKeyStates[SDL_SCANCODE_UP] && !currentKeyStates[SDL_SCANCODE_LEFT] && !currentKeyStates[SDL_SCANCODE_RIGHT])
        {
            player.y += 32;
        }
        else if (currentKeyStates[SDL_SCANCODE_LEFT] && player.x > 0 && canMoveLeft && !currentKeyStates[SDL_SCANCODE_DOWN] && !currentKeyStates[SDL_SCANCODE_UP] && !currentKeyStates[SDL_SCANCODE_RIGHT])
        {
            player.x -= 32;
        }
        else if (currentKeyStates[SDL_SCANCODE_RIGHT] && player.x < SCREEN_WIDTH - 32 && canMoveRight && !currentKeyStates[SDL_SCANCODE_DOWN] && !currentKeyStates[SDL_SCANCODE_LEFT] && !currentKeyStates[SDL_SCANCODE_UP])
        {
            player.x += 32;
        }
    }
    if (!(numFrames % 2))
    {
        move_minotaur(playerSpotted);
    }
    if (minotaurFrame.x < 32 * (NUM_FRAMES_MINOTAUR - 1))
    {
        minotaurFrame.x += 32;
    }
    else
    {
        minotaurFrame.x = 0;
    }
    numFrames++;
    return (interval);
}

void place_torches(int number)
{
    int row = 0;
    row = rand() % 24;
    int col = 0;
    col = rand() % 32;

    if (number > 0)
    {
        if ((torchMap[row][col] == 0) && (row >= 3) && (row <= 20) && (col >= 3) && (col <= 28))
        {
            torchMap[row][col] = 1;
            torchMap[row - 1][col] = 2;
            torchMap[row - 2][col] = 2;
            torchMap[row - 3][col] = 2;
            torchMap[row + 1][col] = 2;
            torchMap[row + 2][col] = 2;
            torchMap[row + 3][col] = 2;
            torchMap[row][col - 1] = 2;
            torchMap[row - 1][col - 1] = 2;
            torchMap[row - 2][col - 1] = 2;
            torchMap[row + 1][col - 1] = 2;
            torchMap[row + 2][col - 1] = 2;
            torchMap[row - 1][col + 1] = 2;
            torchMap[row - 2][col + 1] = 2;
            torchMap[row + 1][col + 1] = 2;
            torchMap[row + 2][col + 1] = 2;
            torchMap[row][col + 1] = 2;
            torchMap[row][col + 2] = 2;
            torchMap[row][col - 2] = 2;
            torchMap[row - 1][col + 2] = 2;
            torchMap[row + 1][col + 2] = 2;
            torchMap[row - 1][col - 2] = 2;
            torchMap[row + 1][col - 2] = 2;
            torchMap[row][col - 3] = 2;
            torchMap[row][col + 3] = 2;
            place_torches(number - 1);
        }
        else
        {
            place_torches(number);
        }
    }
}

void move_minotaur(int moveTowardsPlunger)
{
    if (!moveTowardsPlunger)
    {
        int randomDirection = rand() % 4;
        if(randomDirection == MINOTAUR_DIRECTION_UP && minotaurCanMoveUp)
        {
            minotaur.y -= 32;
        }
        if (randomDirection == MINOTAUR_DIRECTION_DOWN && minotaurCanMoveDown)
        {
            minotaur.y += 32;
        }
        if(randomDirection == MINOTAUR_DIRECTION_LEFT && minotaurCanMoveLeft)
        {
            minotaur.x -= 32;
        }
        if(randomDirection == MINOTAUR_DIRECTION_RIGHT && minotaurCanMoveRight)
        {
            minotaur.x += 32;
        }
    }
    else
    {
        int moveToPlunger = rand() % 3;
        if (moveToPlunger != 0)
        {
            if (minotaur.x > plunger.x && minotaurCanMoveLeft)
            {
                minotaur.x -= 32;
            }
            else if (minotaur.x < plunger.x && minotaurCanMoveRight)
            {
                minotaur.x += 32;
            }
            else if (minotaur.y > plunger.y && minotaurCanMoveDown)
            {
                minotaur.y -= 32;
            }
            else if (minotaur.y < plunger.y && minotaurCanMoveUp)
            {
                minotaur.y += 32;
            }
        }
        else
        {
            move_minotaur(0);
        }
    }
}