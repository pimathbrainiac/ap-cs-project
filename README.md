# AP CS Project
##The Quest for the Golden Plunger

This is the end of the year project I did for my AP Computer Science class in 2015. The code was very messy, but it worked. I've updated the code to look at least presentable, although there are fewer comments than I would like.

##Objective
* Find the Golden Plunger in the labyrinth before the minotaur eats you!
* There are torches that might help you on your journey. Stepping into their light will lead the way to the plunger, but beware! Stepping into the torches light lets the minotaur know where to find the treasure.

##Controls
* **Arrow Keys** - Move the player character.
* **Escape** - Leave the game.

## Authors
* **Philip Nickerson** - *Lead Programmer* - [PhiNick](https://phinick.net)
* **Brady Bucknell** - *Game Designer, Secondary Programmer*
* **Everest Britt** - *Asset Designer*