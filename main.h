/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.h
 * Author: Philip
 *
 * Created on May 14, 2018, 9:45 PM
 */

#ifndef MAIN_H
#define MAIN_H

//minotaur movement directions
enum minotaurDirection
{
    MINOTAUR_DIRECTION_UP,
    MINOTAUR_DIRECTION_LEFT,
    MINOTAUR_DIRECTION_DOWN,
    MINOTAUR_DIRECTION_RIGHT,
    MINOTAUR_DIRECTION_DEFAULT
};

//This struct gives possible movement directions for any given tile
typedef struct
{
    int moveRight;
    int moveLeft;
    int moveUp;
    int moveDown;
} tileDirections;

#endif /* MAIN_H */

